/****************************************************************************************/
/* 			Make Rainbow - mr.c        VERSION: 1.0.4		 	*/
/*										    	*/
/* This is a simple program to generate extended IRC color code rainbow text.		*/
/* This code is written AS-IS, you may use it as you see fit as long as proper credit  	*/
/* is provided.									    	*/
/*										    	*/
/* Written by MatCat, you can find me on ## on freenode.			    	*/
/****************************************************************************************/

/*
* NOTES:
* On linux you will not see the color code, which is \x03, so it is recommended that you
* pipe output.  On Windows the output should be directly copy and pastable.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int main( int argc, char *argv[] ) {
  //int debugging = 1;	
  int colorCode = 0;			// Initialize color code variable
  int colorDirection = 1;		// 0 is down, 1 is up
  int loopCounter = 0;			// Used for loops
  int argvCounter = 0;			// Used for iterating through arguments
  int textStart = 1;			// beginning of text for argv
  int verboseMode = 1;			// used as a flag for verbose mode
  char colorChar = '\x3';		// IRC color code

  if (argc <= 1) {			// Make sure we have something to work with
    printf("At least one argument required.\n");
    exit(EXIT_FAILURE);
  };

  srand(time(0));			// Helps to make the random number random
  colorCode = rand() % 71 + 16;		// set a random start color
  srand(time(0));
  colorDirection = rand() % 2;		// set a random start direction
  
  // Let's look for optional arguments
  for (argvCounter = textStart;argvCounter < argc; argvCounter++) {
    int argvColor = 0;			// Local variables
    int loopColorDirection = 0;
    int argumentProcessed = 0;		// This is so we can tell if we have run out of arguments.

    /********************************************************************/
    /* Argument: -c [color code]                                        */
    /* 									*/
    /* Instead of a random start color, start at specified [color code] */
    /* corrisponding to extended IRC color code. 			*/
    /********************************************************************/
    if (strcmp(argv[argvCounter],"-c") == 0) {
      argvColor = atoi(argv[argvCounter+1]);
      if (argvColor < 16 || argvColor > 87) {
        printf("Color code must be between 16 and 87\n");
        exit(EXIT_FAILURE);        
      } else {
        colorCode = argvColor;
        argumentProcessed=1;
        textStart+=2;
        argvCounter++;
      }
    }

    /********************************************************************/
    /* Argument: -d [direction(0/1)]                                    */
    /* 									*/
    /* Choose if the gradient direction is down (0), descending         */
    /* the gradient direction is going up (1) ascending. 	        */
    /********************************************************************/
    if (strcmp(argv[argvCounter],"-d") == 0) {
      loopColorDirection = atoi(argv[argvCounter+1]);
      if (loopColorDirection < 0 || loopColorDirection > 1) {
        printf("Directions are 0 for down, and 1 for up.\n");
        exit(EXIT_FAILURE);        
      } else {
        colorDirection = loopColorDirection;
        argumentProcessed=1;
        textStart+=2;
        argvCounter++;
      }
    }

    /********************************************************************/
    /* Argument: -q		                                        */
    /* 									*/
    /* Turns of verbose messages and only output the program output.    */
    /********************************************************************/
    if (strcmp(argv[argvCounter],"-q") == 0) {
	verboseMode = 0;
        argumentProcessed=1;
        textStart+=1;
    }

    /********************************************************************/
    /* Argument: -h / --help		                                */
    /* 									*/
    /* Turns of verbose messages and only output the program output.    */
    /********************************************************************/
    if (strcmp(argv[argvCounter],"-h") == 0 || strcmp(argv[argvCounter],"--help") == 0) {
      printf("Usage: %s <arguments> [output]\n\nArguments:\n\t-c [color code]\t\tSpecify a starting color\n\t-d [direction]\t\tSpecify a gradient direction, 0 down, 1 up\n\t-h/--help\t\tYou already know what this does\n\t-q\t\t\tQuiet Mode, only show output",argv[0]);
      exit(0);
    }
    if (debugging) printf("argumentProcessed: %i\ttextStart: %d\tArgument: %s\n",argumentProcessed,textStart,argv[argvCounter]);
    if (argumentProcessed == 0) {break;}
  }

  if (verboseMode) {
    printf("Starting with color code: %i ", colorCode);  
    if (colorDirection) printf("direction UP.\n"); else printf("direction DOWN.\n");
  }

  for (argvCounter = textStart;argvCounter < argc; argvCounter++) {
    for (loopCounter = 0;loopCounter < strlen(argv[argvCounter]);loopCounter++) {
      printf("%c%i%c",colorChar,colorCode, argv[argvCounter][loopCounter]);
      if (colorDirection) {		// Going up
        colorCode+=12; 
	if (colorCode > 87) {		// We need to reverse direction
          colorCode-=11;
	  colorDirection = 0;
          if (colorCode == 88) colorCode = 76;
        }
      } else {				// Going down
        colorCode-=12;
	if (colorCode < 16) {		// We need to reverse direction
          colorCode+=13;
	  colorDirection = 1;
          if (colorCode == 27) colorCode = 16;
        }
      } 
    }
    printf(" ");
  } 
  printf("\n");
}
